function get_case_div_by_coordinates (row, col) {
    return $("#map").find("tr").eq(row).find("td").eq(col);
}

function get_case_div_by_index (idx) {
    nb_rows = $("#map").find("tr").length;
    nb_cols = $("#map").find("td").length / nb_rows;
    coordinate = clic(idx, nb_rows, nb_cols);
    
    return get_case_div_by_coordinates(coordinate.row, coordinate.col);
}

function clic (idx, nb_rows, nb_cols) {
    // Convert Linear Index to Coordinates
    row = parseInt(idx / nb_cols);
    col =  idx % nb_cols;

    if (row > nb_rows) {
        console.log("We have a problem here...");
        row = nb_rows-1;
    }

    return { row, col };
}

function get_player_name(player_rank){
    //player rank is the rank of the player in the game-leaderboard at the time the query is sent
    // this function return it's name
    return $("#game-leaderboard").find("tr").eq(player_rank + 1).find("td").eq(1).text();
}

function get_player_color(player_rank){
    //player rank is the rank of the player in the game-leaderboard at the time the query is sent
    // this function return it's color
    return $("#game-leaderboard").find("tr").eq(player_rank + 1).find("td").eq(1).attr("class").split(" ")[1];
}

function get_player_color_class(player_color){
    return "gia_guessed-" + player_color;
}