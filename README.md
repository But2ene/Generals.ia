# Generals.ia
Generals.ia is a GreaseMonkey script that aims to offer an enhanced interface for Generals.io to help players

Working on :
- Get websockets sent by generals.io and extract payload
- Parse Websocket payload to get game datas
- Design game model
- Add datas to game model we designed
- Use game model to enhanced interface


Possible features to be implemented :
- Memorized map (in the fog, remember the last visible possessor of a bloc and the number of army in it), Remember position of adversary generals in the fog
- Anticipated map : In fog, update the number of army on blocs
- Alert player when big army endanger general, cities are taken
- Estimate adversary generals positions, gave predictions of position in early encounters
- Analyze other players activities and determine : who has taken a city, who are fighting each other, what percent of ennemy armies are visible to the player, who is weak and has few army left on his general
- Take into account cycles to time attacks on ennemy general

- Develop heuristics to gather and move armies efficiently
- Give the 'Gather' order to armies close to an arbitrary position (feature with two clicks, one to choose gathering point, one to choose the radius of effective order
- Automatic moves : expand, protect a city, attack a position

- save stats about games played (activity, towns/lands/army stats, add a replay page ?)
