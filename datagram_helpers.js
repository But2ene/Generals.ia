// Global variables for map patching. It's ugly, I know.
var _map_for_patching = [];

function parse_websocket(payload){
    // Websocket's datagram payload follow a quite simple form, it begins with a number :
    // - 2 :  It's a PING. This is the only part of the datagram.
    // - 3 :  It's a PONG. This is the only part of the datagram.
    // - 42 : It's a game message : the game message is JSON serialized message that is a 3 long array ;
    //   - Message type : describes the type of the message. Can be either "attack", "game_update", or other...
    //   - Info Object : an object that contains informations for the message's use.
    //   - ?? : A boolean we don't know the use yet.

     // Get the payload type :
    var payload_type = parseInt(payload, 10);
    var payload = payload.slice(payload_type.toString().length);

    // If it's a game message
    if (payload_type == 42) {
        payload = JSON.parse(payload);
        payload = parse_game_message(payload);
    }

    return {
        type: payload_type,
        message: payload
    };
}

function parse_game_message(message){
    // Example : '["chat_message","game_1482355476254tv-WayRJYTTYq75BAQmV",{"username":"Anonymous","text":"coucou","playerIndex":0}]'
    // Example : '["pre_game_start"]'
    // Example : '["queue_update",2,1,90]'
    // Example : '["game_won",null,null]'
    // Example : '["game_start",{"playerIndex":0,"replay_id":"rkrpLu_Ex","chat_room":"game_14823564125458UP3s6etd_6GM2PQAQp1","usernames":["BWAH","smaj"]},null]'
    // Example : '["game_lost",{"killer":1},null]'
    // Example : '["game_start",{"playerIndex":5,"replay_id":"ByBhduO4x","chat_room":"game_1482356908719AKmaaxsfdFkKKLEOAQqf","usernames":["mosed1","Cyrus","capesean","Sing","Anonymous","BWAH","Achim","Sime"]},null]'
    // received_message_types = ["stars", "rank", "queue_update", "game_update", "chat_message", "pre_game_start", "game_won", "game_lost"];
    // sent_messages_type = ["attack", "set_username", "join_private", "stars_and_rank", "set_force_start", "chat_message", "cancel", "play", "leave_game", "clear_moves"]

    var _dispatcher = {
        "game_update" : parse_game_update,
        "game_lost" : parse_game_lost,
        "game_won" : parse_game_won,
        "game_start" : parse_game_start,
	"default" : (function (msg){console.log("[G.IA] Packet dropped : " + msg.toString()); return {};})
    };

    var parser = _dispatcher[message[0]] || _dispatcher['default'];
    var parsed_message = parser(message);
    parsed_message.type = message[0];

    return parsed_message;
}

function parse_game_update(message){
    var game_info = message[1];
    
    var scores = game_info["scores"];
    //scores is a 4 long array;
    // - total is the number of "army" (in the interface) possessed by the player
    // - tiles is the number of "lands" (in the interface) possessed by the player
    // - i is the ID of the player
    // - dead is a boolean. While it's false, the player general is not captured yet
    var turn = game_info["turn"]; // turn in the websocket is twice the value indicated in the turn-counter of interface (it permits to take into account 

    // handle cities_diff
    var cities_diff;
    if (game_info["cities_diff"].length == 1){
        cities_diff = [];
    }
    else{
        cities_diff = game_info["cities_diff"].slice(2);
    }

    var generals = game_info["generals"]; // a list of generals indexes in the map, value is -1 if the general is not in sight
    var attackIndex = game_info["attackIndex"]; // number of turns used by the player

    // Handle map
    if (game_info["map_diff"][0] == 0) {
	_map_for_patching = game_info["map_diff"];
    }
    else {
	_map_for_patching = patch_map(_map_for_patching, game_info["map_diff"]);
    }

    var map = {
        // Map is described an array of 2*rows*cols + 2 length :
        // Indexes 2 and 3 are cols and rows.
        // Indexes i from 4 to rows*cols + 3 are number of armies on case i-4
        // Indexes i from rows*cols + 4 to 2*rows*cols + 3 are an identifier of the case :
        //  -  0 - 8 : player occupying this case, 0-8 are index of the players, these indexes match "i" variable in the "scores" variable
        //  - -1 : free space
        //  - -2 : mountain
        //  - -3 : fog
        //  - -4 : obstacle in fog (since last update, can be either moutain or city yet to be discovered)

        cols : _map_for_patching[2],
        rows : _map_for_patching[3],
        length : _map_for_patching[2] * _map_for_patching[3],
        army : [],
        type : []
    };

    // Process the map
    for (var i = 0 ; i < map.length ; i++) {
	map.army[i] = _map_for_patching[i + 4];
	map.type[i] = _map_for_patching[map.length + i + 4];
    }

    return {
        map,
        scores,
        turn,
        attackIndex,
        cities_diff,
        generals
    };
}

function parse_game_lost(payload){
    // Example : '["game_lost",{"killer":1},null]'
    console.log("[G.IA] Sniff :'(");
    var game_info = payload[1];
    var killer_id = game_info.killer;
    return {killer_id};
}

function parse_game_won(payload){
    // Example : '["game_won",null,null]'
    // nothing interesting known in the payload of a game_won message
    console.log("[G.IA] Heil yeah !");
    return {};
}

function parse_game_start(payload){
    // Example : '["game_start",{"playerIndex":5,"replay_id":"ByBhduO4x","chat_room":"game_1482356908719AKmaaxsfdFkKKLEOAQqf","usernames":["mosed1","Cyrus","capesean","Sing","Anonymous","BWAH","Achim","Sime"]},null]'
    var game_info = payload[1];
    var playerIndex = game_info.playerIndex;
    var ids = {
        replay: game_info.replay_id,
        chatroom: game_info.chat_room
    };

    var usernames = game_info.usernames;
    var number_of_players = usernames.length;
    
    return {
        number_of_players,
        playerIndex,
        usernames,
        ids
    };
}

function patch_map(old_map, diff_map) {
    // This function apply a patch to the map
    // following the diff provided

    var new_map = old_map.slice();

    var i = 0;
    var j = 2; // Start at two, counting the two first values of map that shouldn't be there in the diff
    while (i < diff_map.length) {
	// Move forward in map
	j += diff_map[i];
	i++;

	// If there's command, apply them !
	if (i < diff_map.length) {
	    nb_tiles_to_edit = diff_map[i];
	    i++;

	    for (var k = 0; k < nb_tiles_to_edit; k++) {
		new_map[j] = diff_map[i];
		i++;
		j++;
	    }
	}
    }

    return new_map;
}

function print_raw_map(map) {
    map_cols = map[2];
    map_rows = map[3];
    map_length = map_cols * map_rows;

    j = 0;
    str = "";
    for (var i = 0 ; i < map_length; i++) {
	armies = map[i + 4];
	type = map[map_length + i + 4];

	switch (type) {
	case -4:
	    str += " ! ";
	    break;
	case -3:
	    str += " ? ";
	    break;
	case -2:
	    str += " M ";
	    break;
	case -1:
	    str += " - ";
	    break;
	default:
	    if (armies < 10) {
		str += " " + armies.toString() + " ";
	    }
	    else if (armies < 100) {
		str += " " + armies.toString();
	    }
	    else {
		str += armies.toString();
	    }
	    break;
	}
	
	j++;
	if(j == map_cols) {
	    console.log(str);
	    str = "";
	    j = 0;
	}
    }
}

function print_map(map) {
    j = 0;
    str = "";
    for (var i = 0 ; i < map.length; i++) {
        armies = map.army[i];
        type = map.type[i];

        switch (type) {
            case -4:
                str += " ! ";
                break;
            case -3:
                str += " ? ";
                break;
            case -2:
                str += " M ";
                break;
            case -1:
                str += " - ";
                break;
            default:
                if (armies < 10) {
                    str += " " + armies.toString() + " ";
                }
                else if (armies < 100) {
                    str += " " + armies.toString();
                }
                else {
                    str += armies.toString();
                }
                break;
        }

        j++;
        if(j == map.cols) {
            console.log(str);
            str = "";
            j = 0;
        }
    }
}

function print_map_type(map) {
    j = 0;
    str = "";
    for (var i = 0 ; i < map.length; i++) {
        armies = map.army[i];
        type = map.type[i];

        switch (type) {
            case -4:
                str += " ! ";
                break;
            case -3:
                str += " ? ";
                break;
            case -2:
                str += " M ";
                break;
            case -1:
                str += " - ";
                break;
            default:
                str = " " + type.toString() + " ";
                break;
        }

        j++;
        if(j == map.cols) {
            console.log(str);
            str = "";
            j = 0;
        }
    }
}