// ==UserScript==
// @name                Generals.IA
// @namespace           tag:42nimag@gmail.com,2016-12-20:Generals.IA
// @description         Improve user interface & game design for Generals.IO game
// @version             1
// @grant               none
// @include         http://*.generals.io/*
// @exclude         http://*.generals.io/replays/
// @require             http://code.jquery.com/jquery-1.4.2.min.js
// @require             http://cdnjs.cloudflare.com/ajax/libs/socket.io/1.3.5/socket.io.min.js
// @require             datagram_helpers.js
// @require             user_interface_helpers.js
// @run-at  document-start
// ==/UserScript==

// prevent javascript from being executed twice bacause of Frame
if (window.top !== window.self) {
  return;
}

console.log("[G.IA] start main script");

console.log("[G.IA] Injecting some custom CSS");

$("head").append (
    '<style media="screen" type="text/css"> '
        + '#map td.gia_guessed-red{background-color: #772222 !important}'
        + '#map td.gia_guessed-orange{background-color: #C36422 !important}'
        + '#map td.gia_guessed-blue{background-color: #2846FF !important}'
        + '#map td.gia_guessed-tail{background-color: #005F5F !important}'
        + '#map td.gia_guessed-purple{background-color: #5F005F !important}'
        + '#map td.gia_guessed-green{background-color: #006E00 !important}'
        + '#map td.gia_guessed-darkgreen{background-color: #003700 !important}'
        + '#map td.gia_guessed-maroon{background-color: #371111 !important}'
        + '#map td.gia_general, #map td.gia_city, #map td.gia_mountain{background-blend-mode: multiply;background-position: center;background-repeat: no-repeat;}'
        + '#map td.gia_general{background-image: url("/crown.png");}'
        + '#map td.gia_city{background-image: url("/city.png");}'
        + '#map td.gia_mountain{background-image: url("/mountain.png");}'
    + '</style>'
);


console.log("[G.IA] Adding websocket sniffer");

parse_websocket("");

// save WebSocket sending functions then overwrite it
WebSocket.prototype._send = WebSocket.prototype.send;
WebSocket.prototype.send = function (data) {
    this._send(data);
    this.send = function (data) { this._send(data); game_core_helper({data}); };

    this.addEventListener('message', game_core_helper, false);
    game_core_helper({data});
}

var _persisted_generals = [];
var _persisted_cities = [];
var _persisted_map = {};
var _players = [];

function game_core_helper(msg) {
    // First thing first
    var datas = parse_websocket(msg.data);

    if (datas.type == 42) {
        if (datas.message.type == "game_update") {
            // print_map(datas.message.map);
            // console.log(datas.message);

            if (_players.length == 0){
                //get players color
                var player;
                for (rank = 0; rank < datas.message.scores.length; rank++){
                    player = datas.message.scores[rank];
                    _players[player.i] = {};
                    _players[player.i].color = get_player_color(rank);
                    _players[player.i].name = get_player_name(rank);
                    _players[player.i].id = player.i;
                    _players[player.i].dead = player.dead;
                }
            }

            _persisted_generals = update_generals(datas.message.generals, _persisted_generals);
            // console.log(_persisted_generals);

            _persisted_cities = update_cities(datas.message.cities_diff, _persisted_cities);

            _persisted_map = update_map(datas.message.map, _persisted_map, _players);

            console.log(datas.message);
        }
    }
}

function array_diff(a, b) {
    return a.filter(function (i) {return b.indexOf(i) < 0;});
}

function update_generals(generals_update, persisted_generals) { // add argument : the IDs of dead players, or equivalent information
    // check persisted_generals (used at initialisation)
    if (persisted_generals.length != generals_update.length){
        // fill persisted_generals with -1
        persisted_generals = [];
        for (i = 0; i < generals_update.length; i++){
            persisted_generals.push(-1);
        }
    }

    // Modifying data part
    for (var i=0; i< persisted_generals.length;i++){
        // if the position of a new general is known
        if (generals_update[i] >= 0 && persisted_generals[i] != generals_update[i]){
            persisted_generals[i] = generals_update[i];
        }
    }
    // TODO : take into account that players can die, so we need to erase the place of their general if known
    // solution, use ID of dead players
    var dead_generals = [];

    // Modifying screen part
    for (let ng of persisted_generals) {
        if (ng >= 0) {
            // console.log("[G.IA] General seen : " + ng.toString());
            get_case_div_by_index(ng).addClass("gia_general");
        }
    }
    for (let ng of dead_generals) {
        if (ng >= 0) {
            console.log("[G.IA] General dead : " + ng.toString());
            get_case_div_by_index(ng).removeClass("gia_general");

        }
    }

    return persisted_generals;
}

function update_cities(cities_update, persisted_cities, scores){
    // Modifying data part
    if (cities_update.length > 0){
        for (let nc of cities_update){
            if (persisted_cities.indexOf(nc) == -1){
                persisted_cities.push(nc);
            }
        }
    }

    // Modifying screen part
    for (let nc of persisted_cities){
        get_case_div_by_index(nc).addClass("gia_city");
    }

    return persisted_cities;
}

function update_map(map_update, estimated_map, players){
    // initialisation
    if (!estimated_map.hasOwnProperty("length")){
        estimated_map = map_update;
    }
    // go all over the map once, and do all the updates on persistent map
    for (var i=0; i < estimated_map.length; i++){
        if (map_update.type[i] >= -2 ){
            // tile is visible
            estimated_map.type[i] = map_update.type[i];
            estimated_map.army[i] = map_update.army[i];
        }
        else if (estimated_map.type[i] == map_update.type[i]){
            // never visited tile
            estimated_map.army[i] = map_update.army[i];
        }
        else if (estimated_map.type[i] >= -2) {
            // tile was explored, but it came back in fog
            // add anticipation of number of army here
        }
        else if (estimated_map.type[i] < -2){
            // impossible case (contradiction about moutain and plain in fog
            console.log("[G.IA] Logic error in map update, like moving mountain")
        }
        else {
            // unexpected case
            console.log("[G.IA] Error in map update")
        }
    }
    // TODO : when a player dies, replace its tiles by the ones of the killer

    // Modifying screen part
    for (var i = 0; i < estimated_map.length; i++){
        if (estimated_map.type[i] >= 0 && map_update.type[i] < -2) {
            var fog_color_class = get_player_color_class(players[estimated_map.type[i]].color);
            var army = estimated_map.army[i];
            // get_case_div_by_index(i).addClass(fog_color_class).html(function(index, oldText){
            //     var before = oldText.slice(0,oldText.indexOf(">") + 1);
            //     var after = oldText.slice(oldText.lastIndexOf("<"));
            //     return  before + army.toString() + after;
            //
            // });
        }
        else if (estimated_map.type[i] == -2 && map_update.type[i] < -2){
            // if it's a visited mountain
            get_case_div_by_index(i).addClass("gia_mountain");
        }
    }
    return estimated_map;
}